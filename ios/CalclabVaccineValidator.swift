import Foundation
import CoreImage
import UIKit

var detector: CIDetector?

@objc(CalclabVaccineValidator)
class CalclabVaccineValidator: NSObject {
    func string(from image: UIImage) -> String {
        var qrAsString = ""
        guard let detector = CIDetector(ofType: CIDetectorTypeQRCode,
                                        context: nil,
                                        options: [CIDetectorAccuracy: CIDetectorAccuracyHigh]),
            let ciImage = CIImage(image: image),
            let features = detector.features(in: ciImage) as? [CIQRCodeFeature] else {
                return qrAsString
        }

        for feature in features {
            guard let indeedMessageString = feature.messageString else {
                continue
            }
            qrAsString += indeedMessageString
        }

        return qrAsString
    }
    
    func drawPDFfromURL(url: URL) -> UIImage? {
        guard let document = CGPDFDocument(url as CFURL) else { return nil }
        guard let page = document.page(at: 1) else { return nil }

        let pageRect = page.getBoxRect(.mediaBox)
        let renderer = UIGraphicsImageRenderer(size: pageRect.size)
        let img = renderer.image { ctx in
            UIColor.white.set()
            ctx.fill(pageRect)

            ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height)
            ctx.cgContext.scaleBy(x: 1.0, y: -1.0)

            ctx.cgContext.drawPDFPage(page)
        }

        return img
    }

    
    @objc(findQrCode:withResolver:withRejecter:)
    func findQrCode(url: String, resolve:RCTPromiseResolveBlock,reject:RCTPromiseRejectBlock) -> Void {
        let urlParsed = URL(string: url)
        let image = drawPDFfromURL(url: urlParsed!)!
        let str = string(from: image).trimmingCharacters(in: .whitespacesAndNewlines)
        
        if (str != "") {
            resolve(str)
        } else {
            reject("INVALID_PDF", "INVALID_PDF", nil)
        }
    }
}
