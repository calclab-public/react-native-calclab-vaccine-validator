#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(CalclabVaccineValidator, NSObject)

RCT_EXTERN_METHOD(findQrCode:(NSString)url
                 withResolver:(RCTPromiseResolveBlock)resolve
                 withRejecter:(RCTPromiseRejectBlock)reject)

@end
