import jwtDecode from 'jwt-decode'
import { NativeModules, Platform } from 'react-native'

export interface Vaccine {
  iat: number;
  exp: number;
  nbf: number;
  iss: string;
  sub: string;
  codigo: string;
  paciente: Paciente;
  vacinas: Vacina[];
  vacinasCovid: Vacina[];
  _TOKEN_TYPE_CLAIM_:
    | 'CertVacCovid19'
    | 'CertificadoVacinacao'
    | 'CarteiraVacinacaoDigital';
}

export interface Paciente {
  cpf: string;
  nome: string;
  nascimento: Date;
}

export interface Vacina {
  data: Date;
  nome: string;
  dose: string;
}

const LINKING_ERROR =
  `The package 'react-native-calclab-vaccine-validator' doesn't seem to be linked. Make sure: \n\n` +
  Platform.select({ ios: "- You have run 'pod install'\n", default: '' }) +
  '- You rebuilt the app after installing the package\n' +
  '- You are not using Expo managed workflow\n';

const CalclabVaccineValidator = NativeModules.CalclabVaccineValidator
  ? NativeModules.CalclabVaccineValidator
  : new Proxy(
      {},
      {
        get() {
          throw new Error(LINKING_ERROR);
        },
      }
    );

export function findQrCode(fileUri: string): Promise<string> {
  if (!fileUri) throw new Error('INVALID_FILE_URI');
  return CalclabVaccineValidator.findQrCode(fileUri);
}

export async function isValidVaccineCard(qrCode: string): Promise<boolean> {
  if (!qrCode) throw new Error('INVALID_QR_CODE');
  try {
    const vaccine = jwtDecode<Vaccine>(qrCode);
    return [
      'CertVacCovid19',
      'CertificadoVacinacao',
      'CarteiraVacinacaoDigital',
    ].includes(vaccine._TOKEN_TYPE_CLAIM_);
    // setIsVaccinated(!!isVaccinated);
  } catch {
    throw new Error('INVALID_QR_CODE');
  }
}

export async function decodeVaccineCard(qrCode: string): Promise<Vaccine> {
  if (!qrCode) throw new Error('INVALID_QR_CODE');
  try {
    const vaccine = jwtDecode<Vaccine>(qrCode);
    return vaccine;
  } catch {
    throw new Error('INVALID_QR_CODE');
  }
}
