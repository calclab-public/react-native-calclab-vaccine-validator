package com.reactnativecalclabvaccinevalidator;
import com.tom_roush.pdfbox.contentstream.PDFStreamEngine;
import com.tom_roush.pdfbox.contentstream.operator.Operator;
import com.tom_roush.pdfbox.cos.COSBase;
import com.tom_roush.pdfbox.cos.COSName;
import com.tom_roush.pdfbox.pdmodel.PDDocument;
import com.tom_roush.pdfbox.pdmodel.PDPage;
import com.tom_roush.pdfbox.pdmodel.graphics.PDXObject;
import com.tom_roush.pdfbox.pdmodel.graphics.form.PDFormXObject;
import com.tom_roush.pdfbox.pdmodel.graphics.image.PDImageXObject;

//import javax.imageio.ImageIO;
//import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;


public class ExtractImagesUseCase extends PDFStreamEngine{
  private final String filePath;
  // private final String outputDir;
  private final ArrayList<String> filesOutput = new ArrayList();

  // Constructor
  public ExtractImagesUseCase(String filePath){
    this.filePath = filePath;
    // this.outputDir = outputDir;
  }

  // Execute
  public ArrayList<String> execute(){
    try{
      Log.d("CVVM", "EXECUTE");
      Log.d("CVVM", "EXECUTE" + filePath);
      File file = new File(filePath);
      PDDocument document = PDDocument.load(file);

      Log.d("CVVM", "DOCUMENT");
      Log.d("CVVM", document.getNumberOfPages() + "");

      for(PDPage page : document.getPages()){
        Log.d("CVVM", "PROCESS PAGE");
        processPage(page);
      }

    }catch(IOException e){
      e.printStackTrace();
    }

    return this.filesOutput;
  }

  @Override
  protected void processOperator(Operator operator, List<COSBase> operands) throws IOException{
    String operation = operator.getName();

    if("Do".equals(operation)){
      COSName objectName = (COSName) operands.get(0);
      PDXObject pdxObject = getResources().getXObject(objectName);

      if(pdxObject instanceof PDImageXObject){
        // Image
        PDImageXObject image = (PDImageXObject) pdxObject;
        Bitmap bImage = image.getImage();

        Log.d("CVVM", "FOUND IMAGE");


        // File
        String randomName = UUID.randomUUID().toString();
        File outputFile = File.createTempFile(randomName, "jpg");
        outputFile.deleteOnExit();

        FileOutputStream stream = new FileOutputStream(outputFile);

        bImage.compress(Bitmap.CompressFormat.JPEG, 70, stream);

        stream.close();

//       File outputFile = new File(outputDir,randomName + ".png");

        // Write image to file
//        ImageIO.write(bImage, "PNG", outputFile);

        // Add to array
        String uri = Uri.fromFile(outputFile).toString();
        Log.d("CVVM", uri);

        filesOutput.add(uri);

      }else if(pdxObject instanceof PDFormXObject){
        PDFormXObject form = (PDFormXObject) pdxObject;
        showForm(form);
      }
    }

    else super.processOperator(operator, operands);
  }
}
