package com.reactnativecalclabvaccinevalidator;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.module.annotations.ReactModule;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.tom_roush.pdfbox.android.PDFBoxResourceLoader;

import androidx.annotation.NonNull;

import java.io.InputStream;
import java.util.ArrayList;

@ReactModule(name = CalclabVaccineValidatorModule.NAME)
public class CalclabVaccineValidatorModule extends ReactContextBaseJavaModule {
    public static final String NAME = "CalclabVaccineValidator";
    private ReactApplicationContext context;
//    private RNBarcodeDetector mBarcodeDetector;

    public CalclabVaccineValidatorModule(ReactApplicationContext reactContext) {
        super(reactContext);
        PDFBoxResourceLoader.init(reactContext);
        this.context = reactContext;
//        mBarcodeDetector = new RNBarcodeDetector(reactContext);
    }

    @Override
    @NonNull
    public String getName() {
        return NAME;
    }

//    public void loadPDFAndExtractImages (String fileUri) {
//      try{
//        File file = new File(fileUri);
//        PDDocument document = PDDocument.load(file);
//
//        for(PDPage page : document.getPages()){
//          // processPage(page);
//          // EXTRACT IMAGES FROM PDF
//          PDResources pdResources = page.getResources();
//          Map pageImages = pdResources.getImages();
//
//        }
//
//      }catch(final Throwable ex){
//        ex.printStackTrace();
//      }
//    }

    // Example method
    // See https://reactnative.dev/docs/native-modules-android
    @ReactMethod
    public void findQrCode(String fileUrl, final Promise promise) {
      Log.d("CVVM", "START");
      Log.d("CVVM", fileUrl);

      ExtractImagesUseCase useCase = new ExtractImagesUseCase(fileUrl);
      ArrayList<String> fileList = useCase.execute();

      Log.d("CVVM", fileList.size() + " SIZE");
      Log.d("CVVM", fileList.toString());

      String qrCode = null;
      String[] returnArray = new String[fileList.size()];
      returnArray = fileList.toArray(returnArray);

      WritableArray promiseArray = Arguments.createArray();
      for(int i=0;i<returnArray.length;i++){
        try {
        // SCAN BARCODES FROM IMAGE USING ZXING
        InputStream inputStream = this.context.getContentResolver().openInputStream(
          Uri.parse(returnArray[i])
        );

        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
        if (bitmap == null) {
          Log.e("CVVM", "uri is not a bitmap," + returnArray[i]);
          return;
        }
        int width = bitmap.getWidth(), height = bitmap.getHeight();
        int[] pixels = new int[width * height];
        bitmap.getPixels(pixels, 0, width, 0, 0, width, height);
        bitmap.recycle();
        bitmap = null;
        RGBLuminanceSource source = new RGBLuminanceSource(width, height, pixels);
        BinaryBitmap bBitmap = new BinaryBitmap(new HybridBinarizer(source));
        MultiFormatReader reader = new MultiFormatReader();
        try
        {
          Result result = reader.decode(bBitmap);
          qrCode = result.getText();
          Log.d("CVVM", "The content of the QR image is: " + result.getText());
        }
        catch (NotFoundException e)
        {
          Log.e("CVVM", "decode exception", e);
        }
        }catch(final Throwable ex){
          ex.printStackTrace();
        }
      }

      Log.d("CVVM", promiseArray.toString());

      if (qrCode == null) {
        promise.reject("NOT_FOUND", "QrCode Not found");
      } else {
        promise.resolve(qrCode);
      }
    }

    public static native int nativeMultiply
      (int a, int b);
}
